package com.pearadmin.modules.sys.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.configure.proprety.TemplateProperty;
import com.pearadmin.common.constant.CommonConstant;
import com.pearadmin.common.constant.ControllerConstant;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.modules.sys.domain.SysFile;
import com.pearadmin.modules.sys.service.SysFileService;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Describe: 文件控制器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 */
@RestController
@Api(tags = {"资源文件"})
@RequestMapping(ControllerConstant.API_SYSTEM_PREFIX + "file")
public class SysFileController extends BaseController {

    /**
     * 系 统 文 件
     */
    private final String MODULE_PATH = "system/file/";

    /**
     * 配置文件
     */
    @Resource
    private TemplateProperty uploadProperty;

    /**
     * 移 除 服 务
     */
    @Autowired
    private Map<String, SysFileService> fileServiceMap;

    /**
     * 根据配置文件选择实现类
     *
     * @return
     */
    private SysFileService getFileService() {
        SysFileService fileService = null;
        if (uploadProperty.isFtpUse()) {
            fileService = this.fileServiceMap.get("SysFileFTPServiceImpl");
        } else {
            fileService = this.fileServiceMap.get("SysFileServiceImpl");
        }
        return fileService;
    }

    /**
     * Describe: 文件管理页面
     * Param: null
     * Return: ModelAndView
     */
    @GetMapping("main")
    @PreAuthorize("hasPermission('/system/file/main','sys:file:main')")
    public ModelAndView main() {
        return jumpPage(MODULE_PATH + "main");
    }

    /**
     * Describe: 文件资源数据
     * Param: PageDomain
     * Return: 文件资源列表
     */
    @GetMapping("data")
    @PreAuthorize("hasPermission('/system/file/data','sys:file:data')")
    public ResultTable data(PageDomain pageDomain) {
        PageHelper.startPage(pageDomain.getPage(), pageDomain.getLimit());
        PageInfo<SysFile> pageInfo = new PageInfo<>(getFileService().data());
        return pageTable(pageInfo.getList(), pageInfo.getTotal());
    }

    /**
     * Describe: 文件上传视图
     * Param: null
     * Return: 执行结果
     */
    @GetMapping("add")
    @PreAuthorize("hasPermission('/system/file/add','sys:file:add')")
    public ModelAndView add() {
        return jumpPage(MODULE_PATH + "add");
    }

    /**
     * Describe: 文件上传接口
     * Param: SysUser
     * Return: Result
     */
    @PostMapping("upload")
    public Result upload(@RequestParam("file") MultipartFile file) {
        String result = getFileService().upload(file);
        if (Strings.isNotBlank(result)) {
            return Result.success(0, "上传成功", result);
        } else {
            return Result.failure("上传失败");
        }
    }

    /**
     * Describe: 文件上传接口
     * Param: SysUser
     * Return: Result
     */
    @PostMapping("uploadBig")
    public Result upload(@RequestParam("file") MultipartFile file,
                         @RequestParam Long chunkSize,
                         @RequestParam Integer totalNumber,
                         @RequestParam Long chunkNumber,
                         @RequestParam String md5) throws Exception {
        String uploadPath = uploadProperty.getUploadPath();
        //文件存放位置
        String dstFile = String.format("%s\\%s\\%s.%s", uploadPath, md5, md5, StringUtils.getFilenameExtension(file.getOriginalFilename()));
        //上传分片信息存放位置
        String confFile = String.format("%s\\%s\\%s.conf", uploadPath, md5, md5);
        //第一次创建分片记录文件
        //创建目录
        File dir = new File(dstFile).getParentFile();
        if (!dir.exists()) {
            dir.mkdir();
            //所有分片状态设置为0
            byte[] bytes = new byte[totalNumber];
            Files.write(Paths.get(confFile), bytes);
        }
        //随机分片写入文件
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(dstFile, "rw");
             RandomAccessFile randomAccessConfFile = new RandomAccessFile(confFile, "rw");
             InputStream inputStream = file.getInputStream()) {
            //定位到该分片的偏移量
            randomAccessFile.seek(chunkNumber * chunkSize);
            //写入该分片数据
            byte[] data = new byte[1024];
            randomAccessFile.write(toByteArray(inputStream));
            //定位到当前分片状态位置
            randomAccessConfFile.seek(chunkNumber);
            //设置当前分片上传状态为1
            randomAccessConfFile.write(1);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("path", dstFile);
        return Result.success("", result);
    }

    private byte[] toByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024*4];
        int n = 0;
        while (-1 != (n = inputStream.read(buffer))){
            outputStream.write(buffer,0,n);
        }
        return outputStream.toByteArray();
    }

    /**
     * 获取文件分片状态，检测文件MD5合法性
     *
     * @param md5
     * @return
     * @throws Exception
     */
    @PostMapping("/checkFile")
    public Result uploadBig(@RequestParam String md5) throws Exception {
        String UPLOAD_PATH = uploadProperty.getUploadPath();
        String uploadPath = String.format("%s\\%s\\%s.conf", UPLOAD_PATH, md5, md5);
        Path path = Paths.get(uploadPath);
        //MD5目录不存在文件从未上传过
        if (!Files.exists(path.getParent())) {
            return Result.failure("文件未上传");
        }
        //判断文件是否上传成功
        StringBuilder stringBuilder = new StringBuilder();
        byte[] bytes = Files.readAllBytes(path);
        for (byte b : bytes) {
            stringBuilder.append(String.valueOf(b));
        }
        Map<String, Object> result = new HashMap<>();
        //所有分片上传完成计算文件MD5
        if (!stringBuilder.toString().contains("0")) {
            File file = new File(String.format("%s\\%s\\", UPLOAD_PATH, md5));
            File[] files = file.listFiles();
            String filePath = "";
            for (File f : files) {
                //计算文件MD5是否相等
                if (!f.getName().contains("conf")) {
                    filePath = f.getAbsolutePath();
                    try (InputStream inputStream = new FileInputStream(f)) {
                        String md5pwd = DigestUtils.md5DigestAsHex(inputStream);
                        if (!md5pwd.equalsIgnoreCase(md5)) {
                            return Result.failure("文件上传失败");
                        }
                    }
                }
            }
            result.put("path", filePath);
            return Result.success("", result);
        } else {
            //文件未上传完成，反回每个分片状态，前端将未上传的分片继续上传
            result.put("chucks", stringBuilder.toString());
            return Result.success("", result);
        }

    }

    /**
     * Describe: 文件获取接口
     * Param: id
     * Return: 文件流
     */
    @GetMapping("download/{id}")
    public void download(@PathVariable("id") String id) {
        getFileService().download(id);
    }

    /**
     * Describe: 文件删除接口
     * Param: id
     * Return: 文件流
     */
    @DeleteMapping("remove/{id}")
    @PreAuthorize("hasPermission('/system/file/remove','sys:file:remove')")
    public Result remove(@PathVariable("id") String id) {
        boolean result = getFileService().remove(id);
        return Result.decide(result, "删除成功", "删除失败");
    }

    /**
     * Describe: 文件删除接口
     * Param: id
     * Return: 文件流
     */
    @Transactional(rollbackFor = Exception.class)
    @DeleteMapping("batchRemove/{ids}")
    @PreAuthorize("hasPermission('/system/file/remove','sys:file:remove')")
    public Result batchRemove(@PathVariable("ids") String ids) {
        for (String id : ids.split(CommonConstant.COMMA)) {
            getFileService().remove(id);
        }
        return Result.success("删除成功");
    }
}
